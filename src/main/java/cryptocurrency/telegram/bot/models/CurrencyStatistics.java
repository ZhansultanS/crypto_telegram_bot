package cryptocurrency.telegram.bot.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Table(name = "records")
public class CurrencyStatistics implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String currencyName;
    private Long count;

    public CurrencyStatistics(String currencyName, Long count) {
        this.currencyName = currencyName;
        this.count = count;
    }
}
