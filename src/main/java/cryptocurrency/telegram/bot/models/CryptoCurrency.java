package cryptocurrency.telegram.bot.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class CryptoCurrency implements Serializable {
    private String id;
    private String name;
    private String symbol;
    private Double current_price;
    private Long market_cap;
    private Long market_cap_rank;
    private Long total_volume;
    private Double price_change_24h;
    private Double price_change_percentage_24h;
    private String last_updated;
}
