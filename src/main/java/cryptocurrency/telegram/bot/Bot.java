package cryptocurrency.telegram.bot;

import cryptocurrency.telegram.bot.models.CryptoCurrency;
import cryptocurrency.telegram.bot.services.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component
public class Bot extends TelegramLongPollingBot {
    @Value("${bot.username}")
    private String botUsername;

    @Value("${bot.token}")
    private String botToken;

    private final CurrencyService service;

    @Autowired
    public Bot(CurrencyService service) {
        this.service = service;
    }

    @Override
    public void onUpdateReceived(Update update) {
        List<CryptoCurrency> currencies = service.getCryptoCurrencies();
        List<String> currencyNames = new ArrayList<>();
        currencies.forEach(c -> currencyNames.add(c.getName()));

        if (update.hasMessage()) {
            Long chatID = update.getMessage().getChatId();
            String command = update.getMessage().getText();

            switch (command) {
                case "/start" -> {
                    send(chatID, "To see data about cryptocurrency type '/see'");
                    send(chatID, "To see popularity of cryptocurrency in our system type '/stats'");
                }
                case "/see" -> send(chatID, String.join(", ", currencyNames));
                case "/stats" -> {
                    Map<String, Double> stats = service.getStats();
                    StringBuilder output = new StringBuilder();
                    for (String key : stats.keySet()) {
                        output.append(key).append(" = ").append(stats.get(key)).append("\n");
                    }
                    send(chatID, output.toString());
                }
                default -> {
                    for (CryptoCurrency currency : currencies) {
                        if (currency.getName().toLowerCase(Locale.ROOT).equals(command.toLowerCase(Locale.ROOT))) {
                            service.addRecord(currency.getName());
                            send(chatID, currency.toString());
                            return;
                        }
                    }
                    send(chatID, "Sorry, this " + command + " does not exist in our system");
                }
            }
        }
    }

    public void send(Long chatID, String text) {
        SendMessage s = new SendMessage();
        s.setText(text);
        s.setChatId(chatID.toString());
        try {
            execute(s);
        } catch (TelegramApiException e){
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
