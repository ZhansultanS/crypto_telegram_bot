package cryptocurrency.telegram.bot.services;

import cryptocurrency.telegram.bot.models.CryptoCurrency;
import cryptocurrency.telegram.bot.models.CurrencyStatistics;
import cryptocurrency.telegram.bot.repositories.CurrencyStatisticsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class CurrencyService {
    @Value("${crypto.url}")
    private String url;
    private final CurrencyStatisticsRepository repository;
    private final RestTemplate restTemplate;
    private List<CryptoCurrency> cryptoCurrencies;

    @Autowired
    public CurrencyService(CurrencyStatisticsRepository repository) {
        this.repository = repository;
        restTemplate = new RestTemplate();
    }

    public List<CryptoCurrency> getData() {
        ResponseEntity<CryptoCurrency[]> response = restTemplate
                .getForEntity(url, CryptoCurrency[].class);
        CryptoCurrency[] currencies = response.getBody();
        assert currencies != null;
        return Arrays.stream(currencies).toList();
    }

    public void addRecord(String currencyName) {
        currencyName = currencyName.toLowerCase(Locale.ROOT);
        CurrencyStatistics record = repository
                .findByCurrencyName(currencyName)
                .orElse(new CurrencyStatistics(currencyName, 0L));
        record.setCount(record.getCount() + 1);
        repository.save(record);
    }

    @Scheduled(fixedRate = 5000)
    public void getCryptoData() {
        System.out.println("After 5 seconds");
        cryptoCurrencies = getData();
    }

    public Map<String, Double> getStats() {
        Double totalCount = 0D;
        for (CurrencyStatistics record : repository.findAll()) {
            totalCount += record.getCount();
        }

        Map<String, Double> map = new HashMap<>();
        for (CryptoCurrency currency: getData()) {
            Double count = (double) repository
                    .findByCurrencyName(currency.getName().toLowerCase(Locale.ROOT))
                    .orElse(new CurrencyStatistics("", 0L))
                    .getCount();
            if (totalCount != 0 && (count / totalCount) != 0) {
                map.put(currency.getName(), (count / totalCount) * 100);
            }
        }
        return map;
    }

    public String getUrl() {
        return url;
    }

    public List<CryptoCurrency> getCryptoCurrencies() {
        return cryptoCurrencies;
    }
}
