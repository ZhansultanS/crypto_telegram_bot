package cryptocurrency.telegram.bot.repositories;

import cryptocurrency.telegram.bot.models.CurrencyStatistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CurrencyStatisticsRepository extends JpaRepository<CurrencyStatistics, Long> {
    Optional<CurrencyStatistics> findByCurrencyName(String currencyName);
}
